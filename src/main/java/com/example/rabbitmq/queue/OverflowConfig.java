package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OverflowConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.overflow.queue}")
    private String OVERFLOW_QUEUE;

    Queue createOverflowQueue(){
        return QueueBuilder.durable(OVERFLOW_QUEUE)
                .maxLength(2)
                .overflow(QueueBuilder.Overflow.rejectPublish)
                //OR
                //.overflow(QueueBuilder.Overflow.dropHead)
                .build();
    }

    @Bean
    AmqpTemplate defaultOverflowExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(OVERFLOW_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createOverflowQueue());
    }
}
