package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PriorityConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.priority.queue}")
    private String PRIORITY_QUEUE;

    //we set max priority = 1 , so priorities are 0 and 1
    Queue createPriorityQueue(){
        return QueueBuilder.durable(PRIORITY_QUEUE)
                .maxPriority(1)
                .build();
    }

    @Bean
    AmqpTemplate defaultPriorityExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(PRIORITY_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createPriorityQueue());
    }
}
