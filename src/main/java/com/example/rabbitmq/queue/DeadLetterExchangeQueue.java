package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeadLetterExchangeQueue {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.dead.letter.exchange.queue}")
    private String DEAD_LETTER_EXCHANGE_QUEUE;

    @Value("${rabbitmq.fanout.exchange}")
    private String FANOUT_EXCHANGE;

    //when message rejected it will be redirected to fanout queues
    Queue createDeadLetterQueue(){
        return QueueBuilder.durable(DEAD_LETTER_EXCHANGE_QUEUE)
                //message rejection reason
                .ttl(5000)
                .deadLetterExchange(FANOUT_EXCHANGE)
                .build();
    }

    @Bean
    AmqpTemplate defaultDeadLetterExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(DEAD_LETTER_EXCHANGE_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createDeadLetterQueue());
    }
}
