package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TTLconfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.ttl.queue}")
    private String TTL_QUEUE;

    Queue createTTLQueue(){
        return QueueBuilder.durable(TTL_QUEUE)
                .ttl(5000)
                .build();
    }

    @Bean
    AmqpTemplate defaultTTLExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(TTL_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createTTLQueue());
    }

}
