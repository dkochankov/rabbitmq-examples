package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeadLetterRouterConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.dead.letter.router.queue}")
    private String DEAD_LETTER_ROUTER_QUEUE;

    @Value("${rabbitmq.default.queue}")
    private String DEFAULT_QUEUE_NAME;

    //when message is rejected it will be redirected to default queue
    Queue createDeadLetterRouterQueue(){
        return QueueBuilder.durable(DEAD_LETTER_ROUTER_QUEUE)
                .ttl(5000) //reject reason
                .deadLetterExchange("")  //using default exchange
                .deadLetterRoutingKey(DEFAULT_QUEUE_NAME) //using default queue as routing key
                .build();
    }

    @Bean
    AmqpTemplate defaultDeadLetterRouterExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(DEAD_LETTER_ROUTER_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createDeadLetterRouterQueue());
    }
}
