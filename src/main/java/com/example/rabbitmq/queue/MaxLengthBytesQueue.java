package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MaxLengthBytesQueue {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.max.length.bytes.queue}")
    private String MAX_LENGTH_BYTES_QUEUE;

    Queue createMaxLengthBytesQueue(){
        return QueueBuilder.durable(MAX_LENGTH_BYTES_QUEUE)
                .maxLengthBytes(10)
                .build();
    }

    @Bean
    AmqpTemplate defaultMaxLengthBytesExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(MAX_LENGTH_BYTES_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createMaxLengthBytesQueue());
    }
}
