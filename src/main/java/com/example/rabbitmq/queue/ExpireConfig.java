package com.example.rabbitmq.queue;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExpireConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.expire.queue}")
    private String EXPIRE_QUEUE;

    Queue createExpireQueue(){
        return QueueBuilder.durable(EXPIRE_QUEUE)
                .expires(10000)
                .build();
    }

    @Bean
    AmqpTemplate defaultExpireExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setRoutingKey(EXPIRE_QUEUE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createExpireQueue());
    }
}
