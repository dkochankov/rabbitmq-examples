package com.example.rabbitmq.consumer;

import com.example.rabbitmq.model.QueueObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ListenerConsumer {

    private final static String HEADER_X_RETRY = "x-retry";
    private final static int MAX_RETRY = 3;

    @Autowired
    private AmqpTemplate directExchange;

    //implementing retry mechanism:
    @RabbitListener(queues = {"${rabbitmq.direct.queue1}", "${rabbitmq.direct.queue2}"},
            containerFactory = "listenerContainerFactory")
    public void receiveMessages(@Payload QueueObject object, Message message){
        try {
            System.out.println(object);
            throw new RuntimeException();
        } catch(Exception e){
            Integer retryCount = (Integer) message.getMessageProperties().getHeaders().get(HEADER_X_RETRY);

            if(retryCount == null){
                retryCount = 0;
            } else if(retryCount >= MAX_RETRY){
                log.info("message was ignored");
                return;
            }
            log.info("retrying message for {} time", retryCount);
            message.getMessageProperties().getHeaders().put(HEADER_X_RETRY, ++retryCount);
            directExchange.send(message.getMessageProperties().getReceivedRoutingKey(), message);
        }

    }


    //implementation without retry mechanism:
    /*
    @RabbitListener(queues = {"${rabbitmq.direct.queue1}", "${rabbitmq.direct.queue2}"},
    containerFactory = "listenerContainerFactory")
    public void receiveMessages(QueueObject object){
        System.out.println(object);
        throw new RuntimeException();
    }

     */

}
