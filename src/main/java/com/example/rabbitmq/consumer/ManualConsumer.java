package com.example.rabbitmq.consumer;

import com.example.rabbitmq.model.QueueObject;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ManualConsumer {

    @Autowired
    private AmqpAdmin admin;

    @Autowired
    private RabbitTemplate template;

    private int getCountOfMessages(String queueName){
        Properties properties = admin.getQueueProperties(queueName);
        return (int)properties.get(RabbitAdmin.QUEUE_MESSAGE_COUNT);
    }

    public List<QueueObject> receiveMessages(String queueName){
        int count = getCountOfMessages(queueName);
        return IntStream.range(0, count)
                .mapToObj(i -> (QueueObject) template.receiveAndConvert(queueName))
                .collect(Collectors.toList());
    }

}
