package com.example.rabbitmq.config;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HeaderExchangeConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.header.queue1}")
    private String HEADER_QUEUE1;

    @Value("${rabbitmq.header.queue2}")
    private String HEADER_QUEUE2;

    @Value("${rabbitmq.header.exchange}")
    private String HEADER_EXCHANGE;

    Queue createHeaderQueue1(){
        return new Queue(HEADER_QUEUE1, true, false, false);
    }

    Queue createHeaderQueue2(){
        return new Queue(HEADER_QUEUE2, true, false, false);
    }

    HeadersExchange createHeaderExchange(){
        return new HeadersExchange(HEADER_EXCHANGE, true, false);
    }

    //to accept message both error AND debug headers should be contained
    Binding createHeaderBinding1(){
        return BindingBuilder.bind(createHeaderQueue1()).to(createHeaderExchange()).whereAll("error", "debug").exist();
    }

    //to accept message info OR warning headers should be contained
    Binding createHeaderBinding2(){
        return BindingBuilder.bind(createHeaderQueue2()).to(createHeaderExchange()).whereAny("info", "warning").exist();
    }

    @Bean
    public AmqpTemplate headerExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setExchange(HEADER_EXCHANGE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createHeaderQueue1());
        amqpAdmin.declareQueue(createHeaderQueue2());
        amqpAdmin.declareExchange(createHeaderExchange());
        amqpAdmin.declareBinding(createHeaderBinding1());
        amqpAdmin.declareBinding(createHeaderBinding2());
    }
}
