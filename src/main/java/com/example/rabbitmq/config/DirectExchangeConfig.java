package com.example.rabbitmq.config;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DirectExchangeConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.direct.queue1}")
    private String DIRECT_QUEUE1;

    @Value("${rabbitmq.direct.queue2}")
    private String DIRECT_QUEUE2;

    @Value("${rabbitmq.direct.exchange}")
    private String DIRECT_EXCHANGE;

    @Value("${rabbitmq.direct.rooting.key1}")
    private String DIRECT_ROOTING_KEY1;

    @Value("${rabbitmq.direct.rooting.key2}")
    private String DIRECT_ROOTING_KEY2;

    //"DEAD LETTER QUEUE" config
    @Value("${rabbitmq.direct.dead.letter.queue1}")
    private String DIRECT_DEAD_LETTER_QUEUE1;


    Queue createDirectQueue1(){
        //return new Queue(DIRECT_QUEUE1, true, false, false);
        //configuring if exception with DIRECT_QUEUE1 messages to be added to dead letter queue
        return QueueBuilder.durable(DIRECT_QUEUE1)
                .deadLetterExchange("")
                .deadLetterRoutingKey(DIRECT_DEAD_LETTER_QUEUE1)
                .build();
    }

    Queue createDirectQueue2(){
        return new Queue(DIRECT_QUEUE2, true, false, false);
    }

    Queue createDeadLetterQueue1(){
        return new Queue(DIRECT_DEAD_LETTER_QUEUE1, true, false, false);
    }

    DirectExchange createDirectExchange(){
        return new DirectExchange(DIRECT_EXCHANGE, true, false);
    }

    Binding createDirectBinding1(){
        return BindingBuilder.bind(createDirectQueue1()).to(createDirectExchange()).with(DIRECT_ROOTING_KEY1);
    }

    Binding createDirectBinding2(){
        return BindingBuilder.bind(createDirectQueue2()).to(createDirectExchange()).with(DIRECT_ROOTING_KEY2);
    }

    @Bean
    @Primary
    public AmqpTemplate directExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setExchange(DIRECT_EXCHANGE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createDirectQueue1());
        amqpAdmin.declareQueue(createDirectQueue2());
        amqpAdmin.declareQueue(createDeadLetterQueue1());
        amqpAdmin.declareExchange(createDirectExchange());
        amqpAdmin.declareBinding(createDirectBinding1());
        amqpAdmin.declareBinding(createDirectBinding2());
    }


}
