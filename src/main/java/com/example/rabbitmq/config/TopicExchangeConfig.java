package com.example.rabbitmq.config;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicExchangeConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.topic.queue1}")
    private String TOPIC_QUEUE1;

    @Value("${rabbitmq.topic.queue2}")
    private String TOPIC_QUEUE2;

    @Value("${rabbitmq.topic.queue3}")
    private String TOPIC_QUEUE3;

    @Value("${rabbitmq.topic.exchange}")
    private String TOPIC_EXCHANGE;

    @Value("${rabbitmq.topic.pattern1}")
    private String TOPIC_PATTERN1;

    @Value("${rabbitmq.topic.pattern2}")
    private String TOPIC_PATTERN2;

    @Value("${rabbitmq.topic.pattern3}")
    private String TOPIC_PATTERN3;

    Queue createTopicQueue1(){
        return new Queue(TOPIC_QUEUE1, true, false, false);
    }

    Queue createTopicQueue2(){
        return new Queue(TOPIC_QUEUE2, true, false, false);
    }

    Queue createTopicQueue3(){
        return new Queue(TOPIC_QUEUE3, true, false, false);
    }

    TopicExchange createTopicExchange(){
        return new TopicExchange(TOPIC_EXCHANGE, true, false);
    }

    Binding createTopicBinding1(){
        return BindingBuilder.bind(createTopicQueue1()).to(createTopicExchange()).with(TOPIC_PATTERN1);
    }

    Binding createTopicBinding2(){
        return BindingBuilder.bind(createTopicQueue2()).to(createTopicExchange()).with(TOPIC_PATTERN2);
    }

    Binding createTopicBinding3(){
        return BindingBuilder.bind(createTopicQueue3()).to(createTopicExchange()).with(TOPIC_PATTERN3);
    }

    @Bean
    public AmqpTemplate topicExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setExchange(TOPIC_EXCHANGE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createTopicQueue1());
        amqpAdmin.declareQueue(createTopicQueue2());
        amqpAdmin.declareQueue(createTopicQueue3());
        amqpAdmin.declareExchange(createTopicExchange());
        amqpAdmin.declareBinding(createTopicBinding1());
        amqpAdmin.declareBinding(createTopicBinding2());
        amqpAdmin.declareBinding(createTopicBinding3());
    }
}
