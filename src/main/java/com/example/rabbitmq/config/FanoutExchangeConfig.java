package com.example.rabbitmq.config;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FanoutExchangeConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Value("${rabbitmq.fanout.queue1}")
    private String FANOUT_QUEUE1;

    @Value("${rabbitmq.fanout.queue2}")
    private String FANOUT_QUEUE2;

    @Value("${rabbitmq.fanout.exchange}")
    private String FANOUT_EXCHANGE;

    Queue createFanoutQueue1(){
        return new Queue(FANOUT_QUEUE1, true, false, false);
    }

    Queue createFanoutQueue2(){
        return new Queue(FANOUT_QUEUE2, true, false, false);
    }

    FanoutExchange createFanoutExchange(){
        return new FanoutExchange(FANOUT_EXCHANGE, true, false);
    }

    Binding createFanoutBinding1(){
        return BindingBuilder.bind(createFanoutQueue1()).to(createFanoutExchange());
    }

    Binding createFanoutBinding2(){
        return BindingBuilder.bind(createFanoutQueue2()).to(createFanoutExchange());
    }

    @Bean
    public AmqpTemplate fanoutExchange(ConnectionFactory connectionFactory, MessageConverter messageConverter){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter);
        template.setExchange(FANOUT_EXCHANGE);
        return template;
    }

    @PostConstruct
    public void init(){
        amqpAdmin.declareQueue(createFanoutQueue1());
        amqpAdmin.declareQueue(createFanoutQueue2());
        amqpAdmin.declareExchange(createFanoutExchange());
        amqpAdmin.declareBinding(createFanoutBinding1());
        amqpAdmin.declareBinding(createFanoutBinding2());
    }

}
