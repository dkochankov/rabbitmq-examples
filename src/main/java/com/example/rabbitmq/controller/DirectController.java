package com.example.rabbitmq.controller;

import com.example.rabbitmq.model.QueueObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class DirectController {

    @Autowired
    private AmqpTemplate directExchange;

    @Value("${rabbitmq.direct.rooting.key1}")
    private String DIRECT_ROOTING_KEY1;

    @Value("${rabbitmq.direct.rooting.key2}")
    private String DIRECT_ROOTING_KEY2;

    @PostMapping("direct/{key}")
    public ResponseEntity<?> sendMessageWithDirectExchange(@PathVariable int key){
        QueueObject object = new QueueObject("direct", LocalDateTime.now());
        String routingKey = key == 1 ? DIRECT_ROOTING_KEY1 : DIRECT_ROOTING_KEY2;
        directExchange.convertAndSend(routingKey, object);
        return ResponseEntity.ok(true);
    }


}
