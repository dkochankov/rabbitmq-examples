package com.example.rabbitmq.controller;

import com.example.rabbitmq.consumer.ManualConsumer;
import com.example.rabbitmq.model.QueueObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class ManualConsumerController {

    @Autowired
    private ManualConsumer manualConsumer;

    @PostMapping("consume/{queueName}")
    public ResponseEntity<?> consumeAllMessagesOfQueue(@PathVariable String queueName){
        return ResponseEntity.ok(manualConsumer.receiveMessages(queueName));
    }
}
