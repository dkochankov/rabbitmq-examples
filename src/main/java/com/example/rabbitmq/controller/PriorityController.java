package com.example.rabbitmq.controller;

import com.example.rabbitmq.model.QueueObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class PriorityController {

    @Autowired
    private AmqpTemplate defaultPriorityExchange;


    @PostMapping("priority")
    public ResponseEntity<?> sendMessageWithDefaultPriorityExchange(@RequestParam(value = "priority",
            required = false, defaultValue = "0") int priority){
        QueueObject object = new QueueObject("priority " + priority, LocalDateTime.now());
        defaultPriorityExchange.convertAndSend(object, (message) -> {
            message.getMessageProperties().setPriority(priority); // here we set the priority of the message to be sent
            return message;
        });
        return ResponseEntity.ok(true);
    }
}
